# Pololetní práce (1. pololetí 2019/2020)

Tento repozitář slouží pro ukládání řešení pololetní práce.

Pro vložení Vašich řešení si, prosím, projekt *forkněte* a nahrávejte řešení
do Vašeho forku. Nový projekt (fork) nastavte jako *Private* (*Visibility*)
a přidejte uživatele `vhostpur` do členů s oprávněními alespoň *Reporter*.

Váš projekt by měl být automaticky nastaven tak, že každý commit (resp. push)
je zkompilován a sám ohlásí chybu.
