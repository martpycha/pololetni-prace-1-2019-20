/*

Napište program, který vytiskne list kalendáře - tabulku s jedním měsícem.

Program bude ovládán argumenty: bez argumentů vypíše aktuální měsíc,
s jedním argumentem vypíše měsíc současného roku a se 2 argumenty pak
kalendář s daným měsícem a rokem.

> java Kalendar
// Kalendar pro soucasny mesic
> java Kalendar 10
// Kalendar pro rijen soucasneho roku
> java Kalendar 2 2005
// Kalendar unora 2005

Kalendář by měl vypadat zhruba takhle:

+----------------------+
| cervenec 2014        |
+----------------------+
| Po Ut St Ct Pa So Ne |
|     1  2  3  4  5  6 |
|  7  8  9 10 11 12 13 |
| 14 15 16 17 18 19 20 |
| 21 22 23 24 25 26 27 |
| 28 29 30 31          |
+----------------------+

Jak lze v Javě zjistit současné datum najděte na internetu.

Rozšíření (1b): Program bude vypisovat 3 měsíce: vybraný měsíc doplněný
kalendářem pro předchozí a následující měsíc.

.----------------------.  .----------------------.  .----------------------.
|       listopad       |  |       prosinec       |  |        leden         |
|----------------------|  |----------------------|  |----------------------|
| Po Ut St Ct Pa So Ne |  | Po Ut St Ct Pa So Ne |  | Po Ut St Ct Pa So Ne |
|                 1  2 |  |  1  2  3  4  5  6  7 |  |           1  2  3  4 |
|  3  4  5  6  7  8  9 |  |  8  9 10 11 12 13 14 |  |  5  6  7  8  9 10 11 |
| 10 11 12 13 14 15 16 |  | 15 16 17 18 19 20 21 |  | 12 13 14 15 16 17 18 |
| 17 18 19 20 21 22 23 |  | 22 23 24 25 26 27 28 |  | 19 20 21 22 23 24 25 |
| 24 25 26 27 28 29 30 |  | 29 30 31             |  | 26 27 28 29 30 31    |
|                      |  |                      |  |                      |
`----------------------'  `----------------------'  `----------------------'

*/

import java.util.Calendar;
public class Kalendar  {
    public static void main(String[] args)  {
        int mesic;
        int rok;
        Calendar now = Calendar.getInstance();
        if (args.length == 1)  {
            mesic = Integer.parseInt(args[0]);
            rok = now.get(Calendar.YEAR);
        } else if (args.length > 1)  {
            mesic = Integer.parseInt(args[0]);
            rok = Integer.parseInt(args[1]);     
        } else {
            mesic = now.get(Calendar.MONTH) + 1; 
            rok = now.get(Calendar.YEAR);
        }
        for (int pocetkalendaru = -1; pocetkalendaru <= 1; pocetkalendaru++) {
            int mesicek;
            int rocnik;
            if (mesic + pocetkalendaru == 0 & mesic == 1)  {
                rocnik = rok - 1;
                mesicek = 12;
            } else if (mesic + pocetkalendaru == 13 & mesic == 12)  {
                rocnik = rok + 1;
                mesicek = 1;
            } else {
                mesicek = ((mesic + pocetkalendaru) % 13);
                rocnik = rok;
            }

            int denvtydnu = denvtydnu(dnyodzacatku(mesicek, rocnik), rocnik, mesicek);
            int denvmesici = denvtydnu;
            int pocetdnu = pocetdnu(mesicek, rocnik, prestupny(rocnik));
            int dnujehodne = dnyodzacatku(mesicek, rocnik);

            ohraniceni();
            radekmesic(mesicek, rocnik);
            ohraniceni();
            System.out.printf("| po ut st ct pa so ne |\n");
            int cislodne = 1;
            for (int radky = 1; radky <= 6; radky++)  {
                System.out.printf("|");
                for (int den = 1; den <= 7; den++)  {
                    if (den + 7 * (radky - 1) < denvmesici)  {
                        System.out.printf("   ");
                        continue;
                    }                
                    if (den + 7 * (radky - 1) == denvmesici)  {
                        System.out.printf("%3d", cislodne);
                        if (cislodne < pocetdnu)  {
                            denvmesici++;
                        }
                        cislodne++; 
                    }
                    if (den + 7 * (radky - 1) > denvmesici)  {
                        for (int konecvypln = denvmesici; konecvypln < 7 * radky; konecvypln++)  {
                            System.out.printf("   ");
                            denvmesici++;
                        }
                        break; 
                    }
                }
                System.out.printf(" |\n");
            }
            ohraniceni();
            System.out.printf("\n");
            }       
    }
    public static void ohraniceni()  {
        System.out.print("o");
        int kolikrat = 22;
        while (kolikrat > 0)  {
            System.out.print("-");
            kolikrat--;
        }
        System.out.printf("o\n");
    }  
    public static int pocetdnu(int mesic, int rok, boolean prestupny)  {
        int pocetdnu;
        if (mesic == 1 || mesic == 3 || mesic == 5 || mesic == 7 || mesic == 8 || mesic == 10 || mesic == 12)  {
            pocetdnu = 31;
        } else if (mesic == 2)  {
            if (prestupny(rok))  {
                pocetdnu = 29;
            } else {
                pocetdnu = 28;
            }   
        } else {
            pocetdnu = 30;
        }
            return pocetdnu;
    }
    public static boolean prestupny(int rok)  {
        if (rok % 4 == 0)  {
            if (rok % 100 == 0)  {
                if (rok % 400 == 0)  {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true; 
            }
        
        } else {
            return false;
        }     
    }
    public static void radekmesic(int mesic, int rocnik)  {
        System.out.print("|    ");
        if (mesic == 1)  {
            System.out.print(" leden  ");
        }
        if (mesic == 2)  {
            System.out.print("  unor  ");
        }
        if (mesic == 3)  {
            System.out.print(" brezen ");
        }
        if (mesic == 4)  {
            System.out.print(" duben  ");
        }
        if (mesic == 5)  {
            System.out.print(" kveten ");
        }
        if (mesic == 6)  {
            System.out.print(" cerven ");
        }
        if (mesic == 7)  {
            System.out.print("cervenec");
        }
        if (mesic == 8)  {
            System.out.print(" srpen  ");
        }
        if (mesic == 9)  {
            System.out.print("  zari  ");
        }
        if (mesic == 10)  {
            System.out.print(" rijen  ");
        }
        if (mesic == 11)  {
            System.out.print("listopad");
        }
        if (mesic == 12)  {
            System.out.print("prosinec");
        }
        System.out.printf(" %4d     |\n", rocnik);
    }  
    public static int denvtydnu(int dnyodzacatku, int rok, int mesic)  {
        int denvtydnu = 0;
        if (dnyodzacatku(mesic, rok) % 7 == 0)  {
            denvtydnu = 5;
        }
        if (dnyodzacatku(mesic, rok) % 7 == 1)  {
            denvtydnu = 6;
        }    
        if (dnyodzacatku(mesic, rok) % 7 == 2)  {
            denvtydnu = 7;
        }    
        if (dnyodzacatku(mesic, rok) % 7 == 3)  {
            denvtydnu = 1;
        }    
        if (dnyodzacatku(mesic, rok) % 7 == 4)  {
            denvtydnu = 2;
        }    
        if (dnyodzacatku(mesic, rok) % 7 == 5)  {
            denvtydnu = 3;
        }
        if (dnyodzacatku(mesic, rok) % 7 == 6)  {
            denvtydnu = 4;
        }           
        return denvtydnu;
    } 
    public static int dnyodzacatku(int mesic, int rok)  {
        int pocet = 0;
        int pocetdnu = 0;
         for (int i = 1; i < mesic; i++)  {
            if (i == 1 || i == 3 || i == 5 || i == 7 || i == 8 || i == 10 || i == 12)  {
                    pocetdnu = 31; 
            }
            if (i == 2)  {
                if (prestupny(rok))  {
                    pocetdnu = 29;
                }  else  {
                    pocetdnu = 28;
                }
            } 
            if (i == 4 || i == 6 || i == 9 || i == 11)  {   
                pocetdnu = 30;
            }
            pocet = pocet + pocetdnu;
        }
        for (int y = 500; y < rok; y++)  {
            if (prestupny(y))  {
                pocet = pocet + 366;
            } else {
                pocet = pocet + 365;
            }
        }
        return pocet;
    }  
}