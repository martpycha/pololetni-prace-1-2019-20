/*

Napište jednoduchý program pro výpočet Ludolfova čísla. Použijte následující úvahu.

Uvažujme čtverec a jemu vepsaný kruh.
Ludolfovo číslo je pak poměrem jejich obsahů.
Pokud budeme náhodně vybírat body ze čtverce, poměr bodů, které
patří do kruhu vůči celkovému počtu bodů bude právě Ludolfovo číslo
(předpokládejme, že dostatečným množství bodů bychom časem pokryli
celou plochu čtverce).  

*/
public class Pi {
    public static void main(String args[]) {
        int r = 20000000;
        long jevectverci = 0;
        long jevkruhu = 0;
        for (long n = 1; n <= 2 * r; n++)  {
            java.util.Random nahoda = new java.util.Random();
            long x = nahoda.nextInt(2*r);
            long y = nahoda.nextInt(2*r);
            if (patridokruhu(r, x-r , y-r))  {
                jevkruhu++;
            }
            jevectverci++;
        }
        double mezikrok = (double) ((double) jevkruhu/jevectverci); 
        double pi = mezikrok * 4;  
        System.out.printf("%.2f", pi);
    }
    public static boolean patridokruhu(long r, long x, long y)  {
        return r * r >= x*x + y*y;
    }
}