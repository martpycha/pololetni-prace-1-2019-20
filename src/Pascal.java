/*

Napište program pro výpočet
[Pascalova trojúhelníku](https://cs.wikipedia.org/wiki/Pascal%C5%AFv_troj%C3%BAheln%C3%ADk).

Uživatel zadá jako parametr programu počet řádků.
V základním řešení není nutné řešit vycentrování výstupu.

java Pascal 6
1
1 1
1 2 1
1 3 3 1
1 4 6 4 1
1 5 10 10 5 1

Rozšíření (1b): Program bude vypisovat Pascalův trojúhelník zarovnaný na střed.

java Pascal 6 
          1
        1   1
      1   2   1
    1   3   3   1
  1   4   6   4   1
1   5  10  10   5   1


*/
public class Pascal {
    public static void main(String args[]) {
        int pocetradku = Integer.parseInt(args[0]);
        long cislo;
        int mezera;
        if (pocetradku > 12)  {
            mezera = 5;
        } else {
            mezera = 3;
        }
            for (int radek = 0; radek <= pocetradku; radek++)  {
                odsazeni(radek, pocetradku, mezera);
                for (int sloupek = 0; sloupek <= radek; sloupek++)  {
                    if (sloupek == 0 || sloupek == radek)  {
                        cislo = 1;
                    }  else  {
                        cislo = (long) (kombinacni(radek - 1, sloupek - 1) + kombinacni(radek - 1, sloupek));
                    }
                        System.out.printf("%d", cislo);
                        zarovnani(cislo, mezera);
                    }
                System.out.printf("\n");
                }                   
            }   
    public static void zarovnani(long cislo, int mezera)  {
        int rad = 0;
        double x = cislo;
            while (rad < mezera)  {
                x = (double) (x / 10);
                if (x < 0.9999)  {
                    break;
                } 
                rad++;
            }
                for (int y = mezera; y > rad; y--)  {
                    System.out.print(" ");
                }            
    }                
    public static void odsazeni(int radek, int pocetradku, int mezera)  {
        int pocetmezer = (pocetradku * ((mezera + 1) / 2)) - (radek * ((mezera + 1) / 2));
        while (pocetmezer > 0)  {
            System.out.printf(" ");
            pocetmezer--;
        }                                      
    } 
    public static long kombinacni(int radek, int sloupek)  {
            long kombinacni;
        if (radek < sloupek || sloupek < 0 || radek < 0)  {
            kombinacni = 0;
        }  else  {
            kombinacni = (long) faktorial(radek)/(faktorial(sloupek) * faktorial(radek-sloupek));
        }     
            return kombinacni;        
    }    
    public static long faktorial(int i)  {
            int x = 1; 
            long faktorial = 1;
            while (x <= i)  {
                faktorial = faktorial * x;
                x++;    
            }
            return faktorial;
    }
}
