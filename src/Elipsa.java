/*

Nakreslete elipsu, pokud je zadána velikost hlavní a vedlejší poloosy.

Např. pro vstup 30 a 6 program vytiskne následující:

java Elipsa 30 6
              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
         XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
   XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
   XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
         XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
               XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

Neřešte problém, že znaky jsou vyšší než širší, ale berte znakovou mřížku
jako čtvercovou.
Pro vykreslení pak využijte definici elipsy.

*/

public class Elipsa {
    public static void main(String args[]) {
        double a = Integer.parseInt(args[0]);
        double b = Integer.parseInt(args[1]);
        for (double y = -b; y <= b; y++)  {
            for (double x = -a; x <= a; x++)  {
                if (nenivelipse(a,b,x,y))  {
                    System.out.print(" ");                        
                } else {
                    System.out.print("X");
                }                   
            }
        System.out.printf("\n");
        } 
    }
    public static boolean nenivelipse(double a, double b, double x, double y)  {
        return mezikrok(x,a) + mezikrok(y,b) >= 1;                       
    }
    public static double mezikrok(double i, double y)  {
        return (i/y) * (i/y);
    }
}
